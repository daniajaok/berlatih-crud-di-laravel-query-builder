<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index','IndexController@index');
Route::post('/kirim','ReadController@kirim');
Route::get('/form','InputBiodataContrller@form');
//input dan form
Route::get('/cast/create','InputController@form');
Route::post('/cast','InputController@store');
//tampil tabel
Route::get('/casttable','IndexController@TampilData');
//detail
Route::get('/cast/{cast_id}','InputController@show');
//update
Route::get('/cast/{cast_id}/edit','UpdateDeleteController@edit');
Route::put('/cast/{cast_id}','UpdateDeleteController@update');
//hapus
Route::delete('/cast/{cast_id}','UpdateDeleteController@destroy');
