<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class UpdateDeleteController extends Controller
{
    //
    public function edit($id){
        $cast=DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }
    public function update($id,Request $request){

        $request->validate(
            [
            'datanama' => 'required',
            'dataumur' => 'required',
            'databio' => 'required',
            ],
            [
            'datanama.required' => 'inputan nama harus diisi/ tidak boleh kosong',
            'dataumur.required' => 'inputan deskripsi harus diisi/ tidak boleh kosong',
            'databio.required'=>'input isikan beberapa kata saja'
            ]
        );

        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['datanama'],
                    'umur' => $request['dataumur'],
                    'bio' => $request['databio']
                ]
            );
            return redirect('/casttable');
    }

    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/casttable');
    }
}
