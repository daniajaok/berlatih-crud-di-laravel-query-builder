<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class InputController extends Controller
{
    public function form(){
        return view('cast.create');
    }
    public function store(Request $request){
        //dd($request->all());//pengetesan
        $request->validate(
            [
            'datanama' => 'required',
            'dataumur' => 'required',
            'databio' => 'required',
            ],
            [
            'datanama.required' => 'inputan nama harus diisi/ tidak boleh kosong',
            'dataumur.required' => 'inputan deskripsi harus diisi/ tidak boleh kosong',
            'databio.required'=>'input isikan beberapa kata saja'
            ]
        );

        DB::table('cast')->insert(
            [
            'nama' => $request['datanama'],
            'umur' => $request['dataumur'],
            'bio' => $request['databio']
            ]
            
        );

        return redirect('/casttable');
    }

    public function show($id){
        $cast=DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }
}
