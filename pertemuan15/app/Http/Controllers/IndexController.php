<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class IndexController extends Controller
{
    //
    public function index()
    {
        return view('halaman.index');
    }
    public function TampilData()
    {
        $cast = DB::table('cast')->get();
        //dd($cast);//testting
        return view('cast.index', compact('cast'));
        
    }
}
