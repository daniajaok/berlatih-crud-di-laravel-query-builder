@extends('layout.master')

@section('judul')
Halaman create cast
@endsection

@section('content')
<form action="/cast" method="post" >
  @csrf
  <label for="fname">nama:</label><br>
  <input type="text" id="fnama" name="datanama" value=""><br>
  <label for="lname">umur:</label><br>
  <input type="text" id="lumur" name="dataumur" value=""><br>
  <label for="lname">bio:</label><br>
  <textarea name="databio" rows="2" cols="50">
  </textarea><br>
  @error('databio')
      {{$message}}<br>
  @enderror
  <input type="submit" value="Submit">
  
</form>
@endsection 