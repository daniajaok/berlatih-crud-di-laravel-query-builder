@extends('layout.master')

@section('judul')
Halaman tabel cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-2 ">Tambah data</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">nama</th>
            <th scope="col">umur</th>
            <th scope="col">bio</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">edit</a>
                    <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                </form>
                </td>
            </tr>
            @empty
            <tr>
                <td>Data Kategori Kosong</td>
            </tr>
            @endforelse
           
        </tbody>
    </table>

@endsection 