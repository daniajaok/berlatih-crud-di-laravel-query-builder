@extends('layout.master')

@section('judul')
Halaman create cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post" >
  @csrf
  @method('put')
  <label for="fname">nama:</label><br>
  <input type="text" value="{{$cast->nama}}" name="datanama" value=""><br>
  <label for="lname">umur:</label><br>
  <input type="text" value="{{$cast->umur}}" name="dataumur" value=""><br>
  <label for="lname">bio:</label><br>
  <textarea  name="databio" rows="2" cols="50">{{$cast->bio}}
  </textarea><br>
  @error('databio')
      {{$message}}<br>
  @enderror
  <input type="submit" value="Submit">
  
</form>
@endsection 